﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MomentumBar : MonoBehaviour
{
    public int currentMomentum;
    public tk2dUIProgressBar progressBar;
    public int maxMomentum = 20;
    public Color barColor;

    public TextMeshPro barText;
    public tk2dBaseSprite textBg;

    private Tweener tweener;

    private void Start()
    {
        barText.color = barColor;
        barText.color = Color.white;
        textBg.color = barColor;

        progressBar.slicedSpriteBar.color = barColor;
    }

    public void SetMomentum(int newMomentum)
    {
        currentMomentum = newMomentum;
        currentMomentum = Helpers.AtMost(currentMomentum, maxMomentum);

        barText.text = currentMomentum.ToString();

        if (tweener != null)
            tweener.Goto(5f);

        float targetValue = (float)currentMomentum / (float)maxMomentum;

        tweener = DOVirtual.Float(progressBar.Value, targetValue, 0.3f, UpdateBar);
    }

    void UpdateBar(float newValue)
    {
        progressBar.Value = newValue;
    }
}
