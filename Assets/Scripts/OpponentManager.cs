﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class OpponentManager : Singleton<OpponentManager>
{
    private Cooldown choosePlayTimer;

    public void Start()
    {
        choosePlayTimer = Helpers.MakeCooldown(1f, this.transform);
    }

    public void BuyMarketCards()
    {
        List<CardObject> marketCards = new List<CardObject>(Game.Instance.cardMarket.GetCastedList<CardObject>());
        marketCards.Shuffle();

        Helpers.StartWhileLoopCounter();
        while (marketCards.Count > 0 && Helpers.WhileLoopTick())
        {
            CardObject toBuy = marketCards.PopFirst();
            if (Game.Instance.CanAfford(Player.Opponent, toBuy.card))
            {
                if(Card.OpponentCanPurchase(toBuy.card.type))
                {
                    if(Game.Instance.CurrentDriveTypes(Player.Opponent).Contains(Card.GetCardTiming(toBuy.card.type)))
                    {
                        Game.Instance.BuyCard(Player.Opponent, toBuy);
                        Game.Instance.FillMarket(3);
                        return;
                    }
                }
            }
        }
    }

    public void ChoosePlay(float howLong)
    {
        if(howLong <= 0f)
        {
            ChoosePlay();
            return;
        }

        choosePlayTimer.callback = ChoosePlay;
        choosePlayTimer.StartCooldown(howLong);
    }

    private void ChoosePlay()
    {
        CardManager.Instance.OpponentPlayCard(GetPlayCard());
    }

    private CardObject GetPlayCard()
    {
        return CardManager.Instance.opponentHandCardObjects.RandomElement();
    }

    internal void StopChoosingPlay()
    {
        choosePlayTimer.Pause();
    }

    public void AddComboCards()
    {
        Card playCard = CardManager.Instance.GetPlayCard(Player.Opponent);

        List<CardObject> hand = CardManager.Instance.opponentHandCardObjects;

        List<CardObject> matchedCards = new List<CardObject>();
        foreach(CardObject co in hand)
        {
            if(co.card.rank == playCard.rank)
                matchedCards.Add(co);
        }

        List<CardObject> flushCards = new List<CardObject>();
        foreach (CardObject co in hand)
        {
            if (co.card.suit == playCard.suit)
                flushCards.Add(co);
        }


        Helpers.StartWhileLoopCounter();
        List<CardObject> straightCards = new List<CardObject>();
        CardRank higher = Card.NextRank(playCard.rank);
        bool finished = false;
        while(!finished && Helpers.WhileLoopTick())
        {
            finished = true;

            if (hand.Any(x => x.card.rank == higher))
            {
                straightCards.Add(hand.First(x => x.card.rank == higher));
                finished = false;
            }
            higher = Card.NextRank(higher);
        }

        CardRank lower = Card.PrevRank(playCard.rank);
        finished = false;
        while (!finished && Helpers.WhileLoopTick())
        {
            finished = true;

            if (hand.Any(x => x.card.rank == lower))
            {
                straightCards.Add(hand.First(x => x.card.rank == lower));
                finished = false;
            }
            lower = Card.PrevRank(lower);
        }

        if (matchedCards.Count >= straightCards.Count && matchedCards.Count >= flushCards.Count)
        {
            AddCards(matchedCards);
            return;
        }

        if (straightCards.Count >= flushCards.Count)
        {
            AddCards(straightCards);
            return;
        }

        AddCards(flushCards);
    }

    public void AddCards(List<CardObject> cardsToAdd)
    {
        foreach(CardObject co in cardsToAdd)
            CardManager.Instance.AddAdditionalCard(co, Player.Opponent);
    }



}
