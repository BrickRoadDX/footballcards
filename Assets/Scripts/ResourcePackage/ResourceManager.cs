﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using System;
using DG.Tweening;

public enum ResourceType
{
    None = 0,
    Coins = 1,
} //Change Max Resource Index

public class ResourceManager : Singleton<ResourceManager>
{
    public static int MAX_RESOURCE_INDEX = 0;

    public List<int> resourceTypeValues;
    public List<TextMeshPro> resourceTypeDisplays;
    public static List<ResourceType> discardResourceTypes = new List<ResourceType>();

    public static string GetResourceSpriteName(ResourceType type)
    {
        switch (type)
        {
            case ResourceType.Coins:
                return "Coin/0";
        }
        return "laurel-crown";
    }

    public static string GetResourceAnimationName(ResourceType type)
    {
        switch (type)
        {
            case ResourceType.Coins:
                return "CoinIdle";
        }
        return "";
    }

    public static Color GetResourceColor(ResourceType type)
    {


        return Color.white;
    }

    public void ResetResources()
    {
        resourceTypeValues.Clear();
        for (int i = 0; i < MAX_RESOURCE_INDEX; i++)
        {
            resourceTypeValues.Add(0);
        }
        UpdateResourceDisplay();
    }

    int oldMilitaryValue = 0;
    public void UpdateResourceDisplay()
    {
        for (int i = 0; i < MAX_RESOURCE_INDEX; i++)
        {
            resourceTypeDisplays[i].text = string.Format("x {0}", resourceTypeValues[i]);
        }
    }

    void ProcessResourceGained(ResourceType type, int count)
    {

    }

    public void SetResource(ResourceType type, int count)
    {
        SpendResource(type, NumResource(type));
        GainResource(type, count, true);
    }

    public List<List<GainedResourceObject>> gainedResourceObjects = new List<List<GainedResourceObject>>();
    public List<List<int>> gainedResouceCounts = new List<List<int>>();
    internal void GainResource(ResourceType type, int count = 1, bool instant = false, Vector3 origin = default(Vector3), bool mainCameraPos = false)
    {
        if (mainCameraPos)
            origin = Helpers.TranslateFromMainWorldToTk2dWorld(origin);

        if (instant)
        {
            resourceTypeValues[((int)type) - 1] += count;
            ProcessResourceGained(type, count);
            UpdateResourceDisplay();
            return;
        }

        if (count > 0)
        {
            GainedResourceObject gro = Helpers.CreateInstance<GainedResourceObject>("GainedResourceObject", this.transform);

            gro.transform.position = Game.Instance.tk2dCamera.ScreenToWorldPoint(Input.mousePosition);

            if (origin != default(Vector3))
                gro.transform.position = origin;

            gainedResouceCounts[((int)type) - 1].Add(count);
            gro.transform.DOMove(resourceTypeDisplays[((int)type) - 1].transform.position, 1.5f).OnComplete(delegate ()
            {
                ProcessNextResource(type);
                if(gainedResourceObjects[(int)type - 1].Contains(gro))
                    gainedResourceObjects[(int)type - 1].Remove(gro);
            }).SetEase(Ease.OutQuint).Play(); //ease.outquint

            Temporary t = gro.GetComponent<Temporary>();
            t.mLifespan = 1.5f;

            gro.SetSprite(GetResourceSpriteName(type));
            gro.SetSpriteAnimation(GetResourceAnimationName(type));
            //SoundManager.Instance.PlaySound("doo dee"); //Pickup10

            gainedResourceObjects[((int)type) - 1].Add(gro);
        }
    }

    public int NumResource(ResourceType type)
    {
        int total = 0;

        foreach (int i in gainedResouceCounts[((int)type) - 1])
            total += i;

        return resourceTypeValues[((int)type) - 1] + total;
    }

    int resouceIndex;
    void ProcessNextResource(ResourceType type)
    {
        SoundManager.Instance.PlaySound("Pickup10"); //Pickup10

        resouceIndex = ((int)type) - 1;

        if (gainedResouceCounts[resouceIndex].Count < 1)
            return;

        resourceTypeValues[resouceIndex] += gainedResouceCounts[resouceIndex][0];
        ProcessResourceGained(type, gainedResouceCounts[resouceIndex][0]);
        gainedResouceCounts[resouceIndex].RemoveAt(0);
        resourceTypeDisplays[resouceIndex].text = string.Format("x {0}", resourceTypeValues[resouceIndex]);
    }


    public void CleanUpDelayedResourceCounters()
    {
        foreach (List<GainedResourceObject> list in gainedResourceObjects)
        {
            foreach (GainedResourceObject go in list)
            {
                if(go != null)
                    go.gameObject.DestroySelf();
            }
            list.Clear();
        }

        foreach (List<int> list in gainedResouceCounts)
        {
            list.Clear();
        }
    }

    public void Init()
    {
        discardResourceTypes.Clear();

        if (gainedResouceCounts.Count < 1)
        {
            for (int i = 0; i < MAX_RESOURCE_INDEX; i++)
                gainedResouceCounts.Add(new List<int>());
        }

        if (gainedResourceObjects.Count < 1)
        {
            for (int i = 0; i < MAX_RESOURCE_INDEX; i++)
                gainedResourceObjects.Add(new List<GainedResourceObject>());
        }

        CleanUpDelayedResourceCounters();

        ResetResources();
    }

    internal void SpendResource(ResourceType type, int count, bool showAlert = true)
    {
        resourceTypeValues[((int)type) - 1] -= count;

        UpdateResourceDisplay();
    }

    public bool CanAfford(Cost cost)
    {
        for (int i = 0; i < MAX_RESOURCE_INDEX; i++)
        {
            if (NumResource((ResourceType)(i + 1)) < cost.values[i])
            {
                return false;
            }
        }

        return true;
    }

    public void Pay(Cost cost, Action donePayingCallback = null)
    {
        for (int i = 0; i < MAX_RESOURCE_INDEX; i++)
        {
            SpendResource((ResourceType)(i + 1), cost.values[i]);
        }

        UpdateResourceDisplay();
    }

    public void Gain(Cost cost)
    {
        for (int i = 0; i < MAX_RESOURCE_INDEX; i++)
        {
            GainResource((ResourceType)(i + 1), cost.values[i]);
        }

        UpdateResourceDisplay();
    }

}
