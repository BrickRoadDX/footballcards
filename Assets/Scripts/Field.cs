﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : Singleton<Field>
{
    public List<FieldLine> downDistanceFieldLines = new List<FieldLine>();

    // Start is called before the first frame update
    void Start()
    {
        FieldLine fl;
        int fieldPos = -20;

        fl = Helpers.CreateInstance<FieldLine>("FieldLine", this.transform);
        fl.SetFieldLineNumber(0);
        fl.transform.SetLocalPositionX(0f);
        fl.transform.SetLocalPositionY(-21.16f);
        fieldPos += 20;

        for (int i = 0; i <= 5; i++)
        {
            fl = Helpers.CreateInstance<FieldLine>("FieldLine", this.transform);
            fl.SetFieldLineNumber(i * 10);
            fl.transform.SetLocalPositionX(0f);
            fl.transform.SetLocalPositionY(fieldPos);
            fieldPos += 10;
        }

        for (int i = 4; i >= 0; i--)
        {
            fl = Helpers.CreateInstance<FieldLine>("FieldLine", this.transform);
            fl.SetFieldLineNumber(i * 10);
            fl.transform.SetLocalPositionX(0f);
            fl.transform.SetLocalPositionY(fieldPos);
            fieldPos += 10;
        }

        fieldPos += 20;

        fl = Helpers.CreateInstance<FieldLine>("FieldLine", this.transform);
        fl.SetFieldLineNumber(0);
        fl.transform.SetLocalPositionX(0f);
        fl.transform.SetLocalPositionY(121.16f);
    }

    FieldLine CreateFieldLine(float yardLine, Color lineColor, string name = "FieldLine", bool front = false)
    {
        FieldLine fl = Helpers.CreateInstance<FieldLine>("FieldLine", this.transform);
        fl.SetFieldLineNumber(0);
        fl.transform.SetLocalPositionX(0f);
        fl.transform.SetLocalPositionY(yardLine);
        fl.spriteRenderer.color = lineColor;
        fl.spriteRenderer.sortingOrder = front ? 2 : 0;
        fl.gameObject.name = name;
        return fl;
    }

    public void ClearFieldLines()
    {
        foreach (FieldLine line in downDistanceFieldLines)
        {
            line.gameObject.DestroySelf();
        }

        downDistanceFieldLines.Clear();
    }

    public void UpdateFieldLines()
    {
        ClearFieldLines();

        downDistanceFieldLines.Add(CreateFieldLine(Game.Instance.firstDownLine, Helpers.HexToColor("B9AF37AA"), "FirstDownLine", true));
        downDistanceFieldLines.Add(CreateFieldLine(Game.Instance.lineOfScrimmage, Helpers.HexToColor("5A5AF6AA"), "LineOfScrimmage", true));
    }
}
