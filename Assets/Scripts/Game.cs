﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public enum InputState
{
    None = 0,
    Playing = 1,
    CastingCard = 2,
    Discarding = 3, //not used yet
    OpponentCard = 4,
    PlayComboCards = 5,
    ShowComboCards = 6,
    PlayKickCards = 7,
    ShowKickCards = 8,
}

public enum UnitType
{
    None = 0,
    Offense = 1,
    Defense = 2,
}

public enum PlayType
{
    None = 0,
    Run = 1,
    Pass = 2,
    PlaceKick = 3,
    Punt = 4,
}

public class Game : Singleton<Game> {

    public Camera mainCamera;
    public Camera tk2dCamera;
    public Transform cameraHolder;

    public float lineOfScrimmage = 20f;
    public float firstDownLine = 30f;
    public int down = 1;

    const int QUARTER_SECONDS = 180;
    public int timeRemaining = 0;
    public int playClock = 0;
    public int quarter = 1;
    public Cooldown clockTimer;

    public float progressPosition;

    List<FieldLine> fieldLines = new List<FieldLine>();

    public InputState inputState = InputState.None;

    public Transform football;
    public CardType showcaseCardOverride = CardType.None;
    public int startingMomentum = 0;

    public tk2dUIItem confirmButton;
    public TextMeshPro progressText;
    public TextMeshPro playerComboTypeText;
    public TextMeshPro opponentComboTypeText;
    public TooltipShower comboDescTooltip;
    public TextMeshPro scoreText;
    public SpriteRenderer progressArrow;
    public TextMeshPro downDistanceText;
    public TextMeshPro confirmButtonText;
    public TextMeshPro actionReminderText;
    public TextMeshPro clockText;
    public TextMeshPro quarterText;
    public TextMeshPro offenseDefenseText;
    public TextMeshPro playClockText;

    public tk2dUIItem deckButton;
    public tk2dUIItem kickButton;

    private Dictionary<Player, int> scores = new Dictionary<Player, int>();
    public Player offensePlayer = Player.You;
    public Player defensePlayer { get { return offensePlayer == Player.You ? Player.Opponent : Player.You; } }

    Player secondHalfTeam = Player.None;

    float ProgressDirection { get { return offensePlayer == Player.You ? 1f : -1f; } }

    public int defenseFatigue = 0;

    public MomentumBar playerMomentum;
    public MomentumBar opponentMomentum;

    public ObjectDisplay cardMarket;
    public ObjectDisplay playerActiveCards;
    public ObjectDisplay opponentActiveCards;

    private void Start()
    {
        scores.Add(Player.You, 0);
        scores.Add(Player.Opponent, 0);

        NewGame();
    }

    private void OnEnable()
    {
        confirmButton.OnClick += HandleConfirmClicked;
        deckButton.OnClick += HandleDeckButtonClicked;
        kickButton.OnClick += HandleKickButtonClicked;
    }

    internal void NewGame()
    {
        clockTimer = Helpers.MakeCooldown(1f, this.transform, false);
        clockTimer.callback = ClockTick;

        cardMarket.DestroyAll();
        playerActiveCards.DestroyAll();
        opponentActiveCards.DestroyAll();

        SetQuarter(1);
        SetTimeRemaining(QUARTER_SECONDS);

        SetScore(Player.You, 0);
        SetScore(Player.Opponent, 0);

        SetOffensePlayer(Helpers.ChanceRoll() ? Player.You : Player.Opponent);
        secondHalfTeam = defensePlayer;
        StartDrive(true);
        SetupPlay(1f);
        CardManager.Instance.Init();
        GoToBaseState();

        SetMomentum(Player.You, startingMomentum);
        SetMomentum(Player.Opponent, startingMomentum);
    }

    void SetOffensePlayer(Player player)
    {
        offensePlayer = player;
        PointProgressTowardsPlayer(defensePlayer);

        ProcessDriveCards();
    }

    void AdjustScore(Player player, int amount)
    {
        SetScore(player, scores[player] + amount);
    }

    void SetScore(Player player, int score)
    {
        scores[player] = score;
        scoreText.text = string.Format("You: {0}\nOpponent: {1}", scores[Player.You], scores[Player.Opponent]);
    }

    void AdjustMomentum(Player player, int amount)
    {
        SetMomentum(player, GetMomentum(player) + amount);
    }

    int GetMomentum(Player player)
    {
        if (player == Player.You)
            return playerMomentum.currentMomentum;

        return opponentMomentum.currentMomentum;
    }

    void SetMomentum(Player player, int momuntum)
    {
        if (player == Player.You)
            playerMomentum.SetMomentum(momuntum);
        else
            opponentMomentum.SetMomentum(momuntum);
    }

    void ClockTick()
    {
        if (playClock > 0)
        {
            SetTimeRemaining(timeRemaining - 1);
            SetPlayClock(playClock - 1);
        }
        
        clockTimer.Reset();
    }

    public bool CanAfford(Player player, Card card)
    {
        return GetMomentum(player) >= Card.GetMomentumCost(card.type);
    }

    internal void BuyCard(Player player, CardObject cardObject)
    {
        ObjectDisplay display = GetGameBreakerListForPlayer(player);

        if (display.Count >= 3)
        {
            Helpers.ShowReminderText("Too many active cards!");
            return;
        }

        AdjustMomentum(player, -Card.GetMomentumCost(cardObject.card.type));
        cardMarket.RemoveObject(cardObject);

        cardObject.transform.localScale = Vector3.one * 0.75f;
        display.AddObject(cardObject);

        if(Card.GetCardTiming(cardObject.card.type) == CardTiming.Immediate)
        {
            Card.ProcessImmediateAbility(cardObject.card.type, player, 1f);
            display.RemoveObject(cardObject, RemoveAnim.CenterAnimation, 2f);
        }
    }

    void SetTimeRemaining(int timeRemaining)
    {
        this.timeRemaining = timeRemaining;
        clockText.text = Helpers.FormatTimeMinutesSeconds(timeRemaining);

        if(timeRemaining <= 0)
        {
            clockTimer.Pause();
        }
    }

    void SetPlayClock(int playClock)
    {
        this.playClock = Helpers.AtLeast(playClock , 0);
        playClockText.text = playClock.ToString();
    }

    void SetQuarter(int quarter)
    {
        this.quarter = quarter;
        quarterText.text = string.Format("QTR: {0}", quarter);
    }

    void GoToNextQuarter()
    {
        SetQuarter(quarter + 1);
        SetTimeRemaining(QUARTER_SECONDS);
        if(quarter > 4)
        {
            SetQuarter(4);
            Helpers.ShowReminderText("Game over!");
            return;
        }
        else if(quarter == 3)
        {
            SetOffensePlayer(secondHalfTeam);
            StartDrive(true);
        }

        Helpers.ShowReminderText("Next quarter!");

        this.CallActionDelayed(delegate ()
        {
            SetupPlay(1f);
        }, 1f);

    }

    void ShowDownDistance()
    {
        string downText = "";

        switch(down)
        {
            case 1:
                downText = "1st";
                break;
            case 2:
                downText = "2nd";
                break;
            case 3:
                downText = "3rd";
                break;
            case 4:
                downText = "4th";
                break;
        }

        downText += " and ";

        int toGo = Mathf.CeilToInt(firstDownLine - lineOfScrimmage);
        toGo *= (int)ProgressDirection;

        downText += toGo.ToString();

        downDistanceText.text = downText;
        Helpers.ShowReminderText(downText);
    }

    public float GetAdvanceYards()
    {
        float advanceYards = CardManager.Instance.GetPlayCard(offensePlayer).Value * ProgressDirection;

        if (OffenseHasBetterCombo() && CardManager.Instance.ExistingCombo(offensePlayer).Count > 1)
            advanceYards = CardManager.Instance.GetBestComboYards(offensePlayer) * ProgressDirection;

        advanceYards += GetActiveCardBonusYards();

        return advanceYards;
    }

    public float GetActiveCardBonusYards()
    {
        float bonusYards = 0f;

        if (HasActiveCard(offensePlayer, CardType.StrongRunner) && CardManager.Instance.GetPlayType() == PlayType.Run)
            bonusYards += 1f;

        if (HasActiveCard(offensePlayer, CardType.StrongPasser) && CardManager.Instance.GetPlayType() == PlayType.Pass)
            bonusYards += 2f;

        if (HasActiveCard(offensePlayer, CardType.BigPlay))
            bonusYards += 5f;

        if (HasActiveCard(offensePlayer, CardType.BigPass) && CardManager.Instance.GetPlayType() == PlayType.Pass)
            bonusYards += 7f;

        if (HasActiveCard(offensePlayer, CardType.BigRun) && CardManager.Instance.GetPlayType() == PlayType.Run)
            bonusYards += 4f;

        if (HasActiveCard(offensePlayer, CardType.SteadyOffense))
            bonusYards += 1f;

        return bonusYards * ProgressDirection;
    }

    public void ProcessComboResult()
    {
        GoToState(InputState.None);

        float advanceYards = GetAdvanceYards();

        if (CardManager.Instance.ExistingCombo(defensePlayer).Count > 1)
            AdjustDefenseFatigue(1);

        gainLossText = GetGainLossText(lineOfScrimmage + advanceYards);

        lineOfScrimmage += advanceYards;

        ProcessPlayResult();
    }

    void SetDefenseFatigue(int fatigue)
    {
        //defenseFatigue = fatigue;
    }

    void AdjustDefenseFatigue(int amount)
    {
        SetDefenseFatigue(defenseFatigue + amount);
    }

    public void ProcessSuccessfulPlayCards()
    {
        if (HasActiveCard(offensePlayer, CardType.BigPlay))
            EndActiveCard(offensePlayer, CardType.BigPlay, RemoveAnim.CenterAnimation);

        if (HasActiveCard(offensePlayer, CardType.BigPass) && CardManager.Instance.GetPlayType() == PlayType.Pass)
            EndActiveCard(offensePlayer, CardType.BigPass, RemoveAnim.CenterAnimation);

        if (HasActiveCard(offensePlayer, CardType.BigRun) && CardManager.Instance.GetPlayType() == PlayType.Run)
            EndActiveCard(offensePlayer, CardType.BigRun, RemoveAnim.CenterAnimation);

        if (HasActiveCard(defensePlayer, CardType.Sack3))
            EndActiveCard(defensePlayer, CardType.Sack3, RemoveAnim.UpAnimation);

        if (HasActiveCard(defensePlayer, CardType.Sack5))
            EndActiveCard(defensePlayer, CardType.Sack5, RemoveAnim.UpAnimation);

        if (HasActiveCard(defensePlayer, CardType.Sack7))
            EndActiveCard(defensePlayer, CardType.Sack7, RemoveAnim.UpAnimation);
    }

    void ProcessUnsuccessfulPlayCards()
    {
        if (HasActiveCard(offensePlayer, CardType.BigPlay))
            EndActiveCard(offensePlayer, CardType.BigPlay, RemoveAnim.UpAnimation);

        if (HasActiveCard(offensePlayer, CardType.BigRun) && CardManager.Instance.GetPlayType() == PlayType.Run)
            EndActiveCard(offensePlayer, CardType.BigRun, RemoveAnim.UpAnimation);

        if (HasActiveCard(offensePlayer, CardType.BigPass) && CardManager.Instance.GetPlayType() == PlayType.Pass)
            EndActiveCard(offensePlayer, CardType.BigPass, RemoveAnim.UpAnimation);
    }

    string gainLossText = "";
    void ProcessPlayResult()
    {
        ProcessSuccessfulPlayCards();

        lineOfScrimmage = Mathf.Clamp(lineOfScrimmage, -19f, 119f);

        football.transform.DOMove(GetSnapPosition(), 1f);

        if (IsTurnover())
        {
            HandleTouchback();

            SwitchSides(false, 0f);
            return;
        }

        if (lineOfScrimmage >= 100f || lineOfScrimmage <= 0f)
        {
            EndPlay();
            return;
        }

        Helpers.ShowReminderText(gainLossText);

        this.CallActionDelayed(EndPlay, 1f);
    }

    bool OffenseHasBetterCombo()
    {
        if (CardManager.Instance.GetPlayCard(Player.You) == null)
            return false;

        List<Card> offenseCombo = CardManager.Instance.ExistingCombo(offensePlayer);
        List<Card> defenseCombo = CardManager.Instance.ExistingCombo(defensePlayer);

        return Card.CompareCombo(offenseCombo, defenseCombo) > 0;
    }

    bool IsFirstDown()
    {
        if (offensePlayer == Player.You)
            return lineOfScrimmage >= firstDownLine;

        return lineOfScrimmage <= firstDownLine;
    }

    void MoveCameraToTarget(Vector3 target)
    {
        cameraHolder.transform.DOKill();

        Vector3 destination = new Vector3(target.x, target.y, -10f);
        cameraHolder.transform.DOMove(destination, 1f);
    }

    void Touchdown()
    {
        Helpers.ShowReminderText("Touchdown!");

        CardManager.Instance.CleanUpLastHand();

        MoveCameraToTarget(GetSnapPosition());

        AdjustScore(offensePlayer, 7);
        SwitchSides(true, 5f);
        Helpers.ShakeTransform(mainCamera.transform, 2f, 5f);
        Field.Instance.ClearFieldLines();

        downDistanceText.text = "";
    }

    void Safety()
    {
        Helpers.ShowReminderText("Safety!");

        CardManager.Instance.CleanUpLastHand();

        MoveCameraToTarget(GetSnapPosition());

        AdjustScore(defensePlayer, 2);
        SwitchSides(true, 5f);
        Helpers.ShakeTransform(mainCamera.transform, 2f, 5f);
        Field.Instance.ClearFieldLines();

        downDistanceText.text = "";
    }

    bool IsTouchdown()
    {
        return DistanceToEndzone() <= 0f;
    }

    void EndPlay()
    {
        if (lineOfScrimmage >= 100f || lineOfScrimmage <= 0f)
        {
            if (IsTouchdown())
                Touchdown();
            else
                Safety();
            return;
        }
        if (IsFirstDown())
        {
            AdjustMomentum(offensePlayer, 2);
            FirstDown();
        }
        else
        {
            down++;

            if (down > 4)
            {
                Helpers.ShowReminderText("Turnover on downs!");
                MoveCameraToTarget(GetSnapPosition());
                SwitchSides(false);
                return;
            }
        }
        SetupPlay();
    }

    void SwitchSides(bool fromThe20 = false, float nextPlayDelay = 1f)
    {
        SetOffensePlayer(offensePlayer == Player.You ? Player.Opponent : Player.You);

        StartDrive(fromThe20);

        this.CallActionDelayed(delegate ()
        {
            SetupPlay(1f);
        }, nextPlayDelay);
    }

    void StartDrive(bool fromThe20)
    {
        cardMarket.DestroyAll();
        FillMarket(3);

        clockTimer.Pause();

        if(fromThe20)
        {
            if (offensePlayer == Player.You)
            {
                lineOfScrimmage = 20f;
                //mainCamera.transform.DORotate(Vector3.zero, 1f);
            }
            else
            {
                lineOfScrimmage = 80f;
                //mainCamera.transform.DORotate(new Vector3(0f, 0f, 180f), 1f);
            }
        }

        FirstDown();
        SetDefenseFatigue(0);
    }

    void SetupPlay(float playSetupTime = 1f)
    {
        if(timeRemaining < 1)
        {
            GoToNextQuarter();
            return;
        }

        MoveCameraToTarget(GetSnapPosition());

        CardManager.Instance.CleanUpLastHand();
        CardManager.Instance.Redraw();

        football.transform.DOMove(GetSnapPosition(), playSetupTime);
        Field.Instance.UpdateFieldLines();
        ShowDownDistance();

        SetPlayClock(20);

        FillMarket(3);

        GoToBaseState();
    }

    void FirstDown()
    {
        down = 1;

        //AdjustDefenseFatigue(1);

        firstDownLine = lineOfScrimmage + (10f * ProgressDirection);
        firstDownLine = Mathf.Clamp(firstDownLine, 0f, 100f);
        Field.Instance.UpdateFieldLines();
    }

    internal void GoToBaseState()
    {
        GoToState(InputState.Playing);
    }

    public void GoToState(InputState state)
    {
        inputState = state;

        bool showComboUI = inputState == InputState.PlayComboCards || inputState == InputState.ShowComboCards;

        confirmButton.gameObject.SetActive(showComboUI);
        progressText.gameObject.SetActive(showComboUI);
        playerComboTypeText.gameObject.SetActive(showComboUI);
        confirmButtonText.text = inputState == InputState.PlayComboCards ? "Done" : gainLossText;

        if (IsTurnover())
            confirmButtonText.text = "TURNOVER!";

        playerComboTypeText.text = "";
        opponentComboTypeText.text = "";

        foreach (CardObject co in CardManager.Instance.handCardObjects)
            co.UpdateDisplay();

        foreach (CardObject co in CardManager.Instance.opponentHandCardObjects)
            co.UpdateDisplay();

        actionReminderText.text = "";
        offenseDefenseText.text = "";

        //progressArrow.gameObject.SetActive(false);
        kickButton.gameObject.SetActive(false);
        deckButton.gameObject.SetActive(false);

        switch (state)
        {
            case InputState.Playing:
                OpponentManager.Instance.BuyMarketCards();

                actionReminderText.text = offensePlayer == Player.You ? "Play an offense card to start the play" : "Play an defense card to start the play";
                offenseDefenseText.text = offensePlayer == Player.You ? "OFFENSE" : "DEFENSE";
                deckButton.gameObject.SetActive(true);

                if (offensePlayer == Player.You)
                    kickButton.gameObject.SetActive(true);
                break;
            case InputState.PlayComboCards:
                actionReminderText.text = offensePlayer == Player.You ? "Add combo cards to gain more yards" : "Add combo cards to make a tackle";
                break;
            case InputState.ShowComboCards:
                Game.Instance.UpdateComboText();
                //progressArrow.gameObject.SetActive(true);
                //progressArrow.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 90f));touch
                playerComboTypeText.color = Color.white;
                opponentComboTypeText.color = Color.white;

                if (OffenseHasBetterCombo())
                    SetPlayerComboTextColor(offensePlayer, Color.yellow);
                else
                    SetPlayerComboTextColor(defensePlayer, Color.yellow);

                break;
            case InputState.PlayKickCards:
                actionReminderText.text = "Make a combo to kick! 10yds/card";
                confirmButtonText.text = "Snap!";
                playerComboTypeText.gameObject.SetActive(true);
                opponentComboTypeText.gameObject.SetActive(false);
                confirmButton.gameObject.SetActive(true);
                UpdateComboText();
                break;
            case InputState.ShowKickCards:
                playerComboTypeText.gameObject.SetActive(true);
                string kickText = KickIsGood() ? "Kick is good!" : "Not enough yards!";
                if (kickType == PlayType.Punt)
                {
                    kickText = string.Format("{0} yard punt!", GetKickYards());
                    playerComboTypeText.text = kickText;
                }
                else
                {
                    playerComboTypeText.text = string.Format("{0} ({1}) - {2}", CardManager.Instance.GetAllComboNames(offensePlayer), GetKickYards(), kickText);
                }

                confirmButtonText.text = "Kick!";
                confirmButton.gameObject.SetActive(true);
                break;
        }
    }

    public void SetPlayerComboTextColor(Player player, Color color)
    {
        if (player == Player.You)
            playerComboTypeText.color = color;

        if (player == Player.Opponent)
            opponentComboTypeText.color = color;
    }

    public void PointProgressTowardsPlayer(Player player)
    {
        Vector3 rotation = player == Player.You ? new Vector3(0f, 0f, 180f) : Vector3.zero;
        progressArrow.transform.DORotate(rotation, 1f);
    }

    public string GetGainLossText(float newBallPos)
    {
        int diff = Mathf.FloorToInt(newBallPos - lineOfScrimmage);
        diff = diff * (int)ProgressDirection;

        if(diff > 0)
        {
            return string.Format("Gain of {0}", diff);
        }

        if (diff == 0)
            return "No Gain";

        return string.Format("Loss of {0}", -diff);
    }

    public Vector3 GetSnapPosition(Vector3 offset = default(Vector3))
    {
        return new Vector3(0f, lineOfScrimmage, 0f) + offset;
    }

    internal bool IsGameOver()
    {
        return false;
    }

    private void Update()
    {
        //if(Input.GetKeyUp(KeyCode.Alpha1))
        //    CardManager.Instance.TryClickCard(1);

        //if (Input.GetKeyUp(KeyCode.Alpha2))
        //    CardManager.Instance.TryClickCard(2);

        //if (Input.GetKeyUp(KeyCode.Alpha3))
        //    CardManager.Instance.TryClickCard(3);

        //if (Input.GetKeyUp(KeyCode.Alpha4))
        //    CardManager.Instance.TryClickCard(4);

        //if (Input.GetKeyUp(KeyCode.Alpha5))
        //    CardManager.Instance.TryClickCard(5);

        if(Input.GetMouseButtonUp(1))
        {
            switch(inputState)
            {
                case InputState.PlayKickCards:
                    if(!kickSequencePlaying)
                        GoToBaseState();
                    break;
            }
        }

        //Debug.Log(tk2dUIManager.Instance.hitUIItem);

#if UNITY_EDITOR
        DebugInput();
#endif
    }

    internal void UpdateComboText()
    {
        progressText.text = "";
        comboDescTooltip.textToShow = "";

        switch(inputState)
        {
            case InputState.PlayKickCards:
                string text = "";
                if (!kickSequencePlaying)
                {
                    if(kickType == PlayType.Punt)
                        text = string.Format("Choose a Suit - Perfect: {0} yards", DistanceToEndzone());
                            else
                        text = string.Format("Choose a Suit - Needed: {0} yards", KickYardsNeeded());
                }
                else
                {
                    if (kickType == PlayType.Punt)
                        text = string.Format("Distance: {0}", GetKickYards());
                    else
                        text = string.Format("Distance: {0} - Needed: {1} yards", GetKickYards(), KickYardsNeeded());
                }

                playerComboTypeText.text = text;
                break;
            default:
                playerComboTypeText.text = string.Format("{0} ({1})", CardManager.Instance.GetComboName(CardManager.Instance.GetBestComboType(Player.You)), CardManager.Instance.ExistingCombo(Player.You).Count);
                opponentComboTypeText.text = string.Format("{0} ({1})", CardManager.Instance.GetComboName(CardManager.Instance.GetBestComboType(Player.Opponent)), CardManager.Instance.ExistingCombo(Player.Opponent).Count);

                if (CardManager.Instance.ExistingCombo(Player.You).Count == 1)
                    playerComboTypeText.text = "";

                if (CardManager.Instance.ExistingCombo(Player.Opponent).Count == 1)
                    opponentComboTypeText.text = "";

                comboDescTooltip.textToShow = CardManager.Instance.GetComboDesc(CardManager.Instance.GetBestComboType(offensePlayer));
                progressText.text = CardManager.Instance.GetBestComboYardsText(offensePlayer);

                if (inputState == InputState.PlayComboCards && offensePlayer != Player.You)
                    progressText.text = "";

                if (inputState == InputState.ShowComboCards && !OffenseHasBetterCombo())
                {
                    progressText.text = "";
                    comboDescTooltip.textToShow = "";
                }
                break;
        }

    }

    void DebugInput()
    {
        if (Input.GetKeyUp(KeyCode.D))
            CardManager.Instance.DrawCard();

        if (Input.GetKeyUp(KeyCode.N))
        {
            CardManager.gameBreakerDeck.Clear();
            NewGame();
        }

        if(Input.GetKeyUp(KeyCode.C))
        {
            Debug.Log(OffenseHasBetterCombo());
        }

        if (Input.GetKeyUp(KeyCode.T))
            SwitchSides(false);

        if (Input.GetKeyUp(KeyCode.M))
        {
            AdjustMomentum(Player.You, 5);
        }

        if(Input.GetKeyUp(KeyCode.O))
        {
            AdjustMomentum(Player.Opponent, 5);
        }

        if (Input.GetKeyUp(KeyCode.F))
        {
            GainYards(10f);
        }

        if (Input.GetKeyUp(KeyCode.U))
            cardMarket.UpdatePositions();

        if (Input.GetKeyUp(KeyCode.S))
            Helpers.RotateGameSpeed();
    }

    void GainYards(float yards)
    {
        lineOfScrimmage += yards * ProgressDirection;
        ProcessPlayResult();
    }

    public void ProcessSnapResult()
    {
        Card offenseCard = CardManager.Instance.GetPlayCard(offensePlayer);
        Card defenseCard = CardManager.Instance.GetPlayCard(defensePlayer);

        AdjustMomentum(defensePlayer, Card.GetMomentumForRank(defenseCard.rank));

        CardManager.Instance.castingCardObject.costText.text = "";
        CardManager.Instance.opponentCastingCardObject.costText.text = "";

        clockTimer.Play();

        switch(offenseCard.GetPlayType())
        {
            case PlayType.Run:
                if (defenseCard.Value <= offenseCard.Value)
                    PlayResult(false, 0, "Run Stuffed!");
                else
                    PlayResult(true, offenseCard.Value, "Successful Run!");
                break;
            case PlayType.Pass:
                if ((offenseCard.Value + defenseCard.Value) >= 15)
                {
                    PlayResult(false, 0, "Pass Incomplete");
                    clockTimer.Pause();
                }
                else
                {
                    PlayResult(true, offenseCard.Value, "Completed Pass!");
                }
                break;
        }
    }

    void PlayResult(bool success, int yardage, string message)
    {
        progressPosition = lineOfScrimmage + yardage;
        Helpers.ShowReminderText(message);

        if(!success)
            ProcessUnsuccessfulPlayCards();

        this.CallActionDelayed(delegate()
        {
            if(success)
            {
                AdjustMomentum(offensePlayer, 1);

                GoToState(InputState.PlayComboCards);
                CardManager.Instance.UpdateAdditionalCardPositions();

                if (CardManager.Instance.handCardObjects.Count > 0)
                    Helpers.ShowReminderText("Add additional cards");
                else
                    HandleConfirmClicked();    
            }
            else
            {
                AdjustMomentum(defensePlayer, 1);

                if (HasSackCard(defensePlayer))
                {
                    ProcessSack();
                }
                else
                {
                    EndPlay();
                }
            }
        }, 1f);
    }

    void HandleConfirmClicked()
    {
        switch(inputState)
        {
            case InputState.PlayComboCards:
                if(offensePlayer == Player.You && CardManager.Instance.additionalCards.Count < 1)
                {
                    ProcessComboResult(); // Offense only played one so no need to counter
                }
                else
                {
                    OpponentManager.Instance.AddComboCards();
                    gainLossText = GetGainLossText(lineOfScrimmage + GetAdvanceYards());
                    GoToState(InputState.ShowComboCards);
                    DisplayComboWinner();
                }
                break;
            case InputState.ShowComboCards:
                ProcessComboResult();
                break;
            case InputState.PlayKickCards:
                //AddRandomKickCards();
                break;
            case InputState.ShowKickCards:
                FinishKick();
                break;
        }
    }

    bool IsTurnover()
    {
        if (CardManager.Instance.ExistingCombo(offensePlayer).Count < 2) // offesne has to add cards for a fumble
            return false;

        bool defenseHasMoreCards = CardManager.Instance.ExistingCombo(defensePlayer).Count - CardManager.Instance.ExistingCombo(offensePlayer).Count >= 1;

        if (HasActiveCard(defensePlayer, CardType.Fumble) && CardManager.Instance.GetPlayType() == PlayType.Run)
            return defenseHasMoreCards;

        if (HasActiveCard(defensePlayer, CardType.Interception) && CardManager.Instance.GetPlayType() == PlayType.Pass)
            return defenseHasMoreCards;

        return false;
    }

    bool HasSackCard(Player player)
    {
        return HasActiveCard(defensePlayer, CardType.Sack3) || HasActiveCard(defensePlayer, CardType.Sack5) || HasActiveCard(defensePlayer, CardType.Sack7);
    }

    void ProcessSack()
    {
        float sackMagnitude = 0f;
        if(HasActiveCard(defensePlayer, CardType.Sack3))
        {
            sackMagnitude = 3f;
            EndActiveCard(defensePlayer, CardType.Sack3);
        }

        if (HasActiveCard(defensePlayer, CardType.Sack5))
        {
            sackMagnitude = 5f;
            EndActiveCard(defensePlayer, CardType.Sack5);
        }

        if (HasActiveCard(defensePlayer, CardType.Sack7))
        {
            sackMagnitude = 7f;
            EndActiveCard(defensePlayer, CardType.Sack7);
        }

        clockTimer.Play();
        gainLossText = GetGainLossText(lineOfScrimmage + (sackMagnitude * -ProgressDirection));
        GainYards(-sackMagnitude);
    }

    public ObjectDisplay GetGameBreakerListForPlayer(Player player)
    {
        return player == Player.You ? playerActiveCards : opponentActiveCards;
    }

    public bool HasActiveCard(Player player, CardType type)
    {
        ObjectDisplay display = GetGameBreakerListForPlayer(player);

        foreach(CardObject co in display.GetCastedList<CardObject>())
        {
            if (co.card.type == type)
                return true;
        }

        return false;
    }

    void EndActiveCard(Player player, CardType type, RemoveAnim removeAnim = RemoveAnim.CenterAnimation)
    {
        ObjectDisplay display = GetGameBreakerListForPlayer(player);

        CardObject toRemove = null;

        foreach (CardObject co in display.GetCastedList<CardObject>())
        {
            if (co.card.type == type)
                toRemove = co;
        }

        if (toRemove != null)
            display.RemoveObject(toRemove, removeAnim);
    }

    void DisplayComboWinner()
    {
        if (IsTurnover())
        {
            string text = CardManager.Instance.GetPlayCard(offensePlayer).IsRun() ? "FUMBLE!" : "INTERCEPTION!";
            Helpers.ShowReminderText(text);
            football.transform.DOMove(GetSnapPosition() + new Vector3(0f, GetAdvanceYards(), 0f), 1f);
            return;
        }

        if (OffenseHasBetterCombo() && CardManager.Instance.ExistingCombo(offensePlayer).Count > 1)
        {
            Helpers.ShowReminderText("Bonus yards!");
        }
        else
        {
            Helpers.ShowReminderText("Great tackle!");
        }
    }

    public UnitType SideOfBall()
    {
        return offensePlayer == Player.You ? UnitType.Offense : UnitType.Defense;
    }

    void HandleDeckButtonClicked()
    {
        cardMarket.DestroyAll();
        FillMarket(3);

        if (defensePlayer == Player.You)
            SetDefenseFatigue(0);

        if(HasActiveCard(Player.You, CardType.KeepWhenBlind))
        {
            EndActiveCard(Player.You, CardType.KeepWhenBlind);
        }
        else
        {
            CardManager.Instance.DiscardHand(Player.You);
        }

        CardManager.Instance.DrawCard(Player.You);
        CardManager.Instance.handCardObjects.Last().HandleCardClicked();
    }

    void HandleKickButtonClicked()
    {
        //PopupManager.Instance.ShowPopupTwoButtons("Punt or Place Kick?", "Punt", Punt, "Place Kick", PlaceKick);

        string text = "Punt or Place Kick";
        text += string.Format("\n\nKick yards needed: {0}", KickYardsNeeded());
        text += string.Format("\nMax punt yards: {0}", DistanceToEndzone());

        text += string.Format("\n\n10 yards per card in flush. Draw 5 cards looking for matches");

        PopupManager.Instance.ShowPopupThreeButtons(text, "Punt", Punt, "Place Kick", PlaceKick, "Cancel", null);
    }

    PlayType kickType = PlayType.None;
    void Punt()
    {
        Helpers.ShowReminderText("PUNT");
        kickType = PlayType.Punt;
        GoToState(InputState.PlayKickCards);
    }

    void PlaceKick()
    {
        Helpers.ShowReminderText("PLACE KICK");
        kickType = PlayType.PlaceKick;
        GoToState(InputState.PlayKickCards);
    }

    Sequence kickSequence;
    public bool kickSequencePlaying = false;
    int cardsToDraw = 0;
    public void AddRandomKickCards()
    {
        kickSequencePlaying = true;

        UpdateComboText();
        //int cardsToDraw = CardManager.Instance.GetHand(offensePlayer).Count;
        cardsToDraw = 5;
        //if (kickType == PlayType.Punt)
        //    cardsToDraw = 6;

        CardManager.Instance.DiscardHand(offensePlayer);

        kickSequence = DOTween.Sequence();
        kickSequence.AppendInterval(0.5f);


        for (int i = 0; i < cardsToDraw; i++)
        {
            kickSequence.AppendCallback(DrawKickCard);
            kickSequence.AppendInterval(0.5f);
        }

        kickSequence.AppendCallback(DrawLastKickCard);
        kickSequence.AppendInterval(0.5f);

        kickSequence.AppendCallback(ProcessKickResult);
    }

    void ProcessKickResult()
    {
        kickSequencePlaying = false;
        GoToState(InputState.ShowKickCards);
    }

    void DrawKickCard()
    {
        CardManager.Instance.DrawCard(offensePlayer);
        CardObject newCard = CardManager.Instance.GetHand(offensePlayer).Last();
        if (CardManager.Instance.GetCombosWithNewCard(newCard, offensePlayer).Contains(ComboType.Flush))
            CardManager.Instance.AddAdditionalCard(newCard, offensePlayer);

        Helpers.ShowReminderText(cardsToDraw.ToString());
        cardsToDraw --;
    }

    void DrawLastKickCard()
    {
        CardManager.Instance.DrawCard(offensePlayer);
        CardManager.Instance.PlayCard(CardManager.Instance.GetHand(offensePlayer).Last());
        Helpers.ShowReminderText("Bonus card");
    }

    public int GetKickYards()
    {
        if (CardManager.Instance.ExistingCombo(offensePlayer).Count < 1)
            return 0;

        if(CardManager.Instance.GetPlayCard(offensePlayer) != null)
            return (CardManager.Instance.ExistingCombo(offensePlayer).Count - 1) * 10 + CardManager.Instance.GetPlayCard(offensePlayer).Value;

        //        return CardManager.Instance.ExistingCombo(offensePlayer).Count * 10 + CardManager.Instance.ExistingCombo(offensePlayer).Last().Value;
        return CardManager.Instance.ExistingCombo(offensePlayer).Count * 10;
    }

    void FinishKick()
    {
        switch(kickType)
        {
            case PlayType.Punt:
                lineOfScrimmage += GetKickYards() * ProgressDirection;
                football.transform.DOMove(GetSnapPosition(), 1f);
                HandleTouchback();
                SwitchSides(false);
                break;
            case PlayType.PlaceKick:
                football.transform.DOMove(GetSnapPosition() + GetKickYards() * (Vector3.up * ProgressDirection), 1f);
                if (KickIsGood())
                {
                    AdjustScore(offensePlayer, 3);
                    Helpers.ShowReminderText("Kick is good!");
                    SwitchSides(true);
                }
                else
                {
                    SwitchSides(false);
                }
                break;
        }
    }

    float KickYardsNeeded()
    {
        return DistanceToEndzone() + 20f;
    }

    float DistanceToEndzone()
    {
        float yardsNeeded = 100f - lineOfScrimmage;
        if (ProgressDirection < 0f)
            yardsNeeded = 0f + lineOfScrimmage;

        return yardsNeeded;
    }

    bool KickIsGood()
    {
        return GetKickYards() > KickYardsNeeded();
    }

    void HandleTouchback()
    {
        if (lineOfScrimmage >= 100f)
            lineOfScrimmage = 80f;

        if (lineOfScrimmage < 0f)
            lineOfScrimmage = 20f;
    }

    void DiscardOldestMarketCard()
    {
        if (cardMarket.Count >= 3)
            cardMarket.RemoveOldestObject(RemoveAnim.Destroy);
    }

    public void FillMarket(int numCards = 6)
    {
        Helpers.StartWhileLoopCounter();
        while(cardMarket.NumItems() < numCards && Helpers.WhileLoopTick())
        {
            AddToMarket();
        }
    }

    public void AddToMarket()
    {
        Card card = new Card(CardManager.GetNextGameBreaker());
        CardObject co = CardManager.CreateCard(this.transform, card);
        cardMarket.AddObject(co);
    }

    public static Color GetPlayerColor(Player player)
    {
        if (player == Player.You)
            return Helpers.HexToColor("0063CC");

        return Helpers.HexToColor("00CC29");
    }

    private void ProcessDriveCards()
    {
        List<CardObject> toRemove = new List<CardObject>();

        foreach(Player p in new List<Player>() { Player.You, Player.Opponent})
        {
            ObjectDisplay display = GetGameBreakerListForPlayer(p);

            foreach (CardObject co in display.GetCastedList<CardObject>())
            {
                switch (Card.GetCardTiming(co.card.type))
                {
                    case CardTiming.Drive:
                        toRemove.Add(co);
                        break;
                    case CardTiming.Offense:
                        if (offensePlayer != p)
                            toRemove.Add(co);
                        break;
                    case CardTiming.Defense:
                        if (defensePlayer != p)
                            toRemove.Add(co);
                        break;
                }
            }

            foreach (CardObject co in toRemove)
                display.RemoveObject(co, RemoveAnim.UpAnimation);
        }
    }

    public List<CardTiming> CurrentDriveTypes(Player player)
    {
        List<CardTiming> list = new List<CardTiming>() { CardTiming.Immediate, CardTiming.Drive, CardTiming.Game };

        if (player == offensePlayer)
            list.Add(CardTiming.Offense);

        if (player == defensePlayer)
            list.Add(CardTiming.Defense);

        return list;
    }
}
