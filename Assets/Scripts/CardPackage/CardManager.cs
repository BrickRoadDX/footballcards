﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;
using Gamelogic.Grids;
using System.Linq;
using TMPro;

public enum Player
{
    None = 0,
    You = 1,
    Opponent = 2,
}

public enum ComboType
{
    None = 0,
    Flush = 1,
    Straight = 2,
    XofAKind = 3,
}

public class CardManager : Singleton<CardManager>
{
    private Vector2 CASTING_CARD_POS = new Vector2(0f, 10.5f);
    private Vector2 OPPONENT_CASTING_CARD_POS = new Vector2(0f, -6.5f);

    public const int MAX_HAND_SIZE = 12;
    public const int STARTING_HAND_SIZE = 5;
    public const float CARD_SPACING = 4f;

    public const float DEFAULT_HAND_Y_POS = 4f;
    public const float OPPONENT_DEFAULT_HAND_Y_POS = -4f;

    public const float CASTING_HAND_Y_POS = 0f;

    public CardObject castingCardObject;
    public CardObject opponentCastingCardObject;

    public Transform handContainer;
    public List<CardObject> handCardObjects;

    public Transform opponentHandContainer;
    public List<CardObject> opponentHandCardObjects;

    public List<Card> playerDeck = new List<Card>();
    public List<Card> discard = new List<Card>();

    public TextMeshPro deckText;
    public TextMeshPro discardText;
    public bool tabletMode = false;

    public Camera tk2dCamera;

    internal bool canRewindCard;

    public float soundDelay = 0f;

    float pressTime = 0f;

    public void Init()
    {
        this.StopDelayedActions();

        handCardObjects.ClearAndDestroyList();
        opponentHandCardObjects.ClearAndDestroyList();
        additionalCards.ClearAndDestroyList();
        additionalOpponentCards.ClearAndDestroyList();
        castingCardObject.DestroySelf();
        castingCardObject = null;

        SetupPlayerDeck();
        discard.Clear();

        Redraw();

        soundDelay = 0f;
    }

    internal ComboType GetBestComboType(Player player)
    {
        return Card.GetBestComboType(ExistingCombo(player));
    }

    internal float GetBestComboYards(Player player)
    {
        ComboType cType = GetBestComboType(player);
        int numCards = ExistingCombo(player).Count;
        switch (cType)
        {
            case ComboType.Flush:
                return GetPlayCard(player).Value + 5f;
            case ComboType.Straight:
                return ExistingComboTotalValue(player);
            case ComboType.XofAKind:
                return ExistingComboTotalValue(player) * numCards;
        }

        if(GetPlayCard(player) != null)
            return GetPlayCard(player).Value;

        return Card.GetHighCardRank(ExistingCombo(player));
    }

    public string GetBestComboYardsText(Player player)
    {
        ComboType cType = GetBestComboType(player);
        int numCards = ExistingCombo(player).Count;
        float comboValue = GetBestComboYards(player);
        int baseValue = 0;
            
        if(GetPlayCard(Game.Instance.offensePlayer) != null)
            baseValue = GetPlayCard(Game.Instance.offensePlayer).Value;

        float bonusYards = Game.Instance.GetActiveCardBonusYards();

        switch (cType)
        {
            case ComboType.Flush:
                if (bonusYards != 0f)
                {
                    return string.Format("{0} + {1} + {2} = {3}", baseValue, 5,  bonusYards, GetBestComboYards(player) + bonusYards);
                }
                else
                {
                    return string.Format("{0} + {1} = {2}", baseValue, 5, GetBestComboYards(player));
                }
            case ComboType.Straight:
                if (bonusYards != 0f)
                {
                    return string.Format("{0} + {1} + {2} = {3}", baseValue, GetBestComboYards(player) - baseValue, bonusYards, GetBestComboYards(player) + bonusYards);
                }
                else
                {
                    return string.Format("{0} + {1} = {2}", baseValue, GetBestComboYards(player) - baseValue + bonusYards, GetBestComboYards(player) + bonusYards); 
                }
            case ComboType.XofAKind:
                if(bonusYards != 0f)
                {
                    return string.Format("{0} * {1} + {2} = {3}", ExistingComboTotalValue(player), numCards, bonusYards, GetBestComboYards(player) + bonusYards);
                }
                else
                {
                    return string.Format("{0} * {1} = {2}", ExistingComboTotalValue(player), numCards, GetBestComboYards(player)); 
                }
        }

        return string.Format("{0} + {1} = {2}", baseValue, bonusYards, GetBestComboYards(player) + bonusYards);
    }

    internal void AddFlushCards(CardSuit suit)
    {
        for(int i = handCardObjects.Count - 1; i >= 0; i--)
        {
            if(handCardObjects.HasElement(i))
            {
                if (handCardObjects[i].card.suit == suit)
                    AddAdditionalCard(handCardObjects[i], Player.You);
            }
        }
    }

    public void SetupPlayerDeck()
    {
        playerDeck.Clear();

        for (int i = 1; i <= 13; i++)
        {
            for(int j = 1; j <= 4; j++)
            {
                playerDeck.Add(new Card(CardType.PlayerCard, (CardSuit)j, (CardRank)i));
            }
        }

        playerDeck.Shuffle();
    }

    public void ReshuffleDiscard()
    {
        playerDeck.AddRange(discard);
        discard.Clear();
        playerDeck.Shuffle();
        Debug.Log(string.Format("Reshuffled {0} cards  " + GetCardListString(playerDeck), playerDeck.Count));
    }

    string GetCardListString(List<Card> cards)
    {
        string cardString = "";
        foreach (Card c in cards)
            cardString += c.GetCardString() + ",";

        cardString = cardString.Trim(',');
        return cardString;
    }

    internal void Redraw()
    {
        DrawUpTo(GetOffenseCards(), Game.Instance.offensePlayer);
        DrawUpTo(GetDefenseCards(), Game.Instance.defensePlayer);
    }

    int GetOffenseCards()
    {
        int numCards = STARTING_HAND_SIZE;

        if (Game.Instance.HasActiveCard(Game.Instance.offensePlayer, CardType.DriveCard))
            numCards++;

        return numCards;
    }

    int GetDefenseCards()
    {
        int numCards = STARTING_HAND_SIZE;

        if (Game.Instance.HasActiveCard(Game.Instance.defensePlayer, CardType.DriveCard))
            numCards++;

        return numCards;
    }

    public void DrawUpTo(int numCards, Player player = Player.You)
    {
        List<CardObject> cardObjects = GetHand(player);

        while (cardObjects.Count < numCards && (playerDeck.Count > 0 || discard.Count > 0))
        {
            DrawCard(player);
        }
    }

    public void DrawCard(Player player = Player.You, Card forceDraw = null)
    {
        Transform handTransform = handContainer;
        List<CardObject> cardObjects = GetHand(player);

        if(player == Player.Opponent)
            handTransform = opponentHandContainer;

        if (cardObjects.Count >= MAX_HAND_SIZE)
        {
            Helpers.ShowReminderText(Helpers.AddIcons("Overdrew!"));
            return;
        }

        if (forceDraw == null && playerDeck.Count < 1) //drawing card from deck
        {
            ReshuffleDiscard();
        }

        Card drawnCard = playerDeck[0];
        playerDeck.RemoveAt(0);

        CardObject co = CreateCard(handTransform, drawnCard);

        cardObjects.Add(co);

        co.SetColliderSize(true);

        UpdateCardPositions();
        UpdateDeckDiscardText();

        if (player == Player.Opponent)
            co.SetHidden(true);
    }

    public static CardObject CreateCard(Transform parent, Card card)
    {
        CardObject co = Helpers.CreateInstance<CardObject>("CardObject", parent);
        co.SetCard(card);
        return co;
    }

    internal void DiscardCard(CardObject cardObject)
    {
        if (cardObject == null)
            return;

        discard.Add(cardObject.card);

        if (castingCardObject == cardObject)
            castingCardObject = null;

        if (opponentCastingCardObject == cardObject)
            opponentCastingCardObject = null;

        if (cardObject.IsOpponentsCard())
            opponentHandCardObjects.Remove(cardObject);
        else
            handCardObjects.Remove(cardObject);

        cardObject.CleanUp();

        handContainer.DOKill();
        handContainer.DOLocalMoveY(DEFAULT_HAND_Y_POS, 0.5f);

        opponentHandContainer.DOKill();
        opponentHandContainer.DOLocalMoveY(OPPONENT_DEFAULT_HAND_Y_POS, 0.5f);

        UpdateCardPositions();

        UpdateDeckDiscardText();
    }

    internal Card GetPlayCard(Player player)
    {
        if (player == Player.You && castingCardObject != null)
            return castingCardObject.card;

        if(player == Player.Opponent && opponentCastingCardObject != null)
            return opponentCastingCardObject.card;

        return null;
    }

    internal bool IsCastingCard()
    {
        return castingCardObject != null;
    }

    public List<float> GetHandPositions(int numCards, float cardSpacing)
    {
        List<float> handPositions = new List<float>();
        if(numCards % 2 != 0)
        {
            handPositions.Add(0f);
            int cardsOutFromMiddle = Mathf.FloorToInt((float)numCards / (float)2);
            for(int i = 0; i < cardsOutFromMiddle; i++)
            {
                float position = (i+1) * cardSpacing;
                handPositions.Insert(0, -position);
                handPositions.Add(+position);
            }
        }
        else
        {
            int cardsOutFromMiddle = Mathf.FloorToInt((float)numCards / (float)2);
            for (int i = 0; i < cardsOutFromMiddle; i++)
            {
                float position = (i+1) * cardSpacing;
                position -= cardSpacing / 2f;
                handPositions.Insert(0, -position);
                handPositions.Add(+position);
            }
        }
        return handPositions;
    }

    public void UpdateCardPositions()
    {
        UpdateCardPositions(Player.You);
        UpdateCardPositions(Player.Opponent);
    }

    public void UpdateCardPositions(Player player)
    {
        List<CardObject> cardObjects = handCardObjects;

        if (player == Player.Opponent)
            cardObjects = opponentHandCardObjects;

        int index = 0;
        int middle = cardObjects.Count / 2;

        List<float> handSpacing = GetHandPositions(cardObjects.Count, CARD_SPACING);

        int cardIndex = 0;
        foreach (CardObject co in cardObjects)
        {
            float posX = handSpacing[cardIndex];

            co.handXpos = posX;
            co.handYpos = 0f;

            co.TweenToPos();
            cardIndex++;
        }
    }

    public void UpdateAdditionalCardPositions()
    {
        int i = 0;
        foreach (CardObject co in additionalCards)
        {
            co.transform.DOKill();
            co.transform.DOLocalMoveX(CASTING_CARD_POS.x + 2f + (i + 1) * 4f, 0.3f);
            co.transform.DOLocalMoveY(CASTING_CARD_POS.y, 0.3f);
            i++;
        }

        i = 0;
        foreach (CardObject co in additionalOpponentCards)
        {
            co.transform.DOKill();
            co.transform.DOLocalMoveX(OPPONENT_CASTING_CARD_POS.x + 2f + (i + 1) * 4f, 0.3f);
            co.transform.DOLocalMoveY(OPPONENT_CASTING_CARD_POS.y, 0.3f);
            i++;
        }

        Game.Instance.UpdateComboText();
    }

    public string GetComboName(ComboType cType)
    {
        switch (cType)
        {
            case ComboType.Flush:
                return "Flush";
            case ComboType.Straight:
                return "Straight";
            case ComboType.XofAKind:
                return "Matched";
        }

        return "";
    }

    public string GetAllComboNames(Player player)
    {
        string s = "";
        List<ComboType> types = Card.GetCombos(ExistingCombo(player));
        types.Reverse();
        foreach (ComboType c in types)
            s += GetComboName(c) + "/";
        s = s.TrimEnd('/');
        return s;
    }

    public string GetComboDesc(ComboType cType)
    {
        switch (cType)
        {
            case ComboType.Flush:
                return "Flush:\nAdd 5 yards";
            case ComboType.Straight:
                return "Straight:\nTotal value of played cards";
            case ComboType.XofAKind:
                return "Matched:\nTotal value of played cards * Number of played cards";
        }

        return "";
    }

    Collider raycastCollider = null;
    CardObject hitCardObject = null;
    public CardObject hoveredCardObject;
    CardObject newHoveredCardObject;
    float middleXpos = 0f;
    void Update()
    {
        if (soundDelay > 0f)
            soundDelay -= Time.deltaTime;

        if (!Game.HasPrivateInstance() || Game.Instance.IsGameOver())
            return;

        if(Input.GetMouseButtonUp(1) && IsCastingCard())
        {
            UndoPlayedCard();
            return;
        }

        raycastCollider = Helpers.RaycastFromMouse(tk2dCamera);
        hitCardObject = null;
        if (raycastCollider != null)
            hitCardObject = raycastCollider.GetComponent<CardObject>();

        if (hitCardObject != null)
        {
            newHoveredCardObject = hitCardObject;

            if (!newHoveredCardObject.IsPlayersCard())
                return;

            if (newHoveredCardObject != null && hoveredCardObject != null && newHoveredCardObject != hoveredCardObject)
            {
                ResetHoveredCard();
            }

            if (hoveredCardObject != newHoveredCardObject)
            {
                if (soundDelay <= 0f)
                    SoundManager.Instance.PlaySound("bip");

                //Game.Instance.ShowHoveredCard(newHoveredCardObject.card.type, null);
            }

            hoveredCardObject = newHoveredCardObject;
            hoveredCardObject.SetVerticalPos(3f);
        }
        else if (hoveredCardObject != null)
        {
            ResetHoveredCard();
        }

        if (Input.GetMouseButtonUp(1) && hoveredCardObject != null)
        {
            DiscardHoveredCard();
            return;
        }

        if(tabletMode)
        {
            if (!Input.GetMouseButton(0) && hoveredCardObject != null)
            {
                middleXpos = hoveredCardObject.transform.localPosition.x;
                pressTime = 0f;
            }

            if (Input.GetMouseButton(0) && hoveredCardObject != null)
            {
                pressTime += Time.deltaTime;
                hoveredCardObject.transform.SetLocalPositionX(middleXpos + (pressTime * Helpers.RandomFloatFromRangeInclusive(-15f, 15f)));

                if (pressTime >= 1f)
                    DiscardHoveredCard();
            }
        }
    }

    public void DiscardHoveredCard()
    {
        pressTime = 0f;

        DiscardCard(hoveredCardObject);
    }

    public bool CanUndo()
    {
        return canRewindCard && Game.Instance.inputState == InputState.CastingCard;
    }

    public void UndoPlayedCard()
    {
        if (IsCastingCard() && CanUndo())
        {
            soundDelay = 0.3f;
            SoundManager.Instance.PlaySound("warning");
            handCardObjects.Add(castingCardObject);
            castingCardObject.SetColliderSize(true);
            castingCardObject.SetRotation(Direction.Down);
            castingCardObject = null;
            handContainer.DOKill();
            handContainer.DOLocalMoveY(DEFAULT_HAND_Y_POS, 0.5f);

            OpponentManager.Instance.StopChoosingPlay();

            Game.Instance.GoToBaseState();
            UpdateCardPositions();
        }
    }

    void ResetHoveredCard()
    {
        hoveredCardObject.SetVerticalPos(0f);
        hoveredCardObject = null;
    }

    public void CleanUpLastHand()
    {
        DiscardCard(castingCardObject);
        DiscardCard(opponentCastingCardObject);

        foreach (CardObject co in additionalCards)
            discard.Add(co.card);

        foreach (CardObject co in additionalOpponentCards)
            discard.Add(co.card);

        additionalCards.ClearAndDestroyList();
        additionalOpponentCards.ClearAndDestroyList();
    }

    void UpdateDeckDiscardText()
    {
        if(deckText != null)
        {
            deckText.text = playerDeck.Count.ToString();
            discardText.text = discard.Count.ToString();
        }
    }

    public void DiscardHand(Player player)
    {
        DiscardTo(0, player);
    }

    internal void DiscardTo(int numCards, Player player)
    {
        List<CardObject> cardObjects = GetHand(player);

        while (cardObjects.Count > numCards)
        {
            DiscardCard(cardObjects[Helpers.RandomIntFromRange(0, cardObjects.Count)]);
        }
    }

    public void PlayCard(CardObject co)
    {
        //Debug.Log("Start casting: " + co.card.GetCardString());

        castingCardObject = co;

        SoundManager.Instance.PlaySound("doo dee");

        co.SetColliderSize(false);
        co.transform.DOKill();
        co.transform.DOLocalMoveX(CASTING_CARD_POS.x, 0.3f);
        co.transform.DOLocalMoveY(CASTING_CARD_POS.y, 0.3f);

        handContainer.DOKill();
        handContainer.DOLocalMoveY(CASTING_HAND_Y_POS, 0.5f);

        handCardObjects.Remove(co);

        canRewindCard = true;
        if(Game.Instance.inputState == InputState.Playing)
        {
            Game.Instance.GoToState(InputState.CastingCard);
            OpponentManager.Instance.ChoosePlay(0.3f);
        }
    }

    public List<CardObject> additionalCards = new List<CardObject>();
    public List<CardObject> additionalOpponentCards = new List<CardObject>();
    List<CardObject> GetAdditionalCardList(Player player)
    {
        return player == Player.You ? additionalCards : additionalOpponentCards;
    }

    public List<CardObject> GetHand(Player player)
    {
        return player == Player.You ? handCardObjects : opponentHandCardObjects;
    }

    public void AddAdditionalCard(CardObject co, Player player)
    {
        co.SetHidden(false);

        List<ComboType> combos = GetCombosWithNewCard(co, player);
        Debug.Log(combos.ListString());
        if(combos.Count < 1 && ExistingCombo(player).Count > 0)
        {
            Debug.Log("No combos");
            return;
        }

        co.SetColliderSize(false);

        SoundManager.Instance.PlaySound("doo dee");

        GetAdditionalCardList(player).Add(co);
        hoveredCardObject = null;

        GetHand(player).Remove(co);

        UpdateAdditionalCardPositions();
        UpdateCardPositions();
    }

    public List<Card> ExistingCombo(Player player)
    {
        List<Card> cards = new List<Card>();

        if(GetPlayCard(player) != null)
            cards.Add(GetPlayCard(player));

        foreach (CardObject co in GetAdditionalCardList(player))
            cards.Add(co.card);

        return cards;
    }

    public int ExistingComboTotalValue(Player player)
    {
        int total = 0;
        foreach (Card c in ExistingCombo(player))
            total += c.Value;

        return total;
    }

    public List<ComboType> GetCombosWithNewCard(CardObject co, Player player)
    {
        List<Card> cards = ExistingCombo(player);
        cards.Add(co.card);

        return Card.GetCombos(cards);
    }

    public bool HasCombosWithNewCard(CardObject co, Player player)
    {
        return GetCombosWithNewCard(co, player).Count > 0;
    }

    internal void RemoveFromAdditionalCards(CardObject co, Player player)
    {
        if (!GetAdditionalCardList(player).Contains(co))
            return;

        co.SetColliderSize(true);

        SoundManager.Instance.PlaySound("dee doo");

        GetAdditionalCardList(player).Remove(co);
        GetHand(player).Add(co);

        UpdateAdditionalCardPositions();
        UpdateCardPositions();
    }

    public void OpponentPlayCard(CardObject co)
    {
        co.SetHidden(false);

        opponentCastingCardObject = co;

        SoundManager.Instance.PlaySound("dee doo");

        co.SetColliderSize(false);
        co.transform.DOKill();
        co.transform.DOLocalMoveX(OPPONENT_CASTING_CARD_POS.x, 0.3f);
        co.transform.DOLocalMoveY(OPPONENT_CASTING_CARD_POS.y, 0.3f);

        opponentHandCardObjects.Remove(co);

        UpdateCardPositions();

        if (Game.Instance.inputState == InputState.CastingCard)
        {
            Game.Instance.GoToState(InputState.OpponentCard);
            this.CallActionDelayed(Game.Instance.ProcessSnapResult, 0.6f);
        }
    }

    public void TryClickCard(int cardNum)
    {
        if(handCardObjects.Count >= cardNum)
        {
            handCardObjects[cardNum - 1].HandleCardClicked();
        }
    }

    public static List<CardType> gameBreakerDeck = new List<CardType>();
    public static CardType GetNextGameBreaker()
    {
        if(gameBreakerDeck.Count < 1)
        {
            gameBreakerDeck = Helpers.GetAllEnumTypes<CardType>();
            gameBreakerDeck.Remove(CardType.PlayerCard);
            gameBreakerDeck.Remove(CardType.None);
            gameBreakerDeck.Shuffle();


#if UNITY_EDITOR
            if (Game.Instance.showcaseCardOverride != CardType.None)
                gameBreakerDeck.Insert(0, Game.Instance.showcaseCardOverride);
#endif
        }

        return gameBreakerDeck.PopFirst();
    }

    public bool IsUsingCard(CardObject co, Player player)
    {
        if(player == Player.You)
        {
            if (additionalCards.Contains(co) || castingCardObject == co)
                return true;
        }
        else if(player == Player.Opponent)
        {
            if (additionalOpponentCards.Contains(co) || opponentCastingCardObject == co)
                return true;
        }

        return false;
    }

    internal PlayType GetPlayType()
    {
        return GetPlayCard(Game.Instance.offensePlayer).GetPlayType();
    }
}
