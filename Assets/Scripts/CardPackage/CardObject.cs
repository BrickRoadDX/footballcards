﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CardObject : MonoBehaviour, SortingOrderObject
{
	public Card card;
	public TextMeshPro mainText;
    public TextMeshPro titleText;
    public TextMeshPro timingText;

	public SpriteRenderer bgSprite;
    public SpriteRenderer suitSprite;
    public TextMeshPro rankText;
    public TextMeshPro numText;
    public TextMeshPro costText;

	public tk2dUIItem uiItem;

	public Transform rotater;

    public BoxCollider cardCollider;

    [HideInInspector]
    public Direction direction = Direction.None;

    private bool extended = false;

    [HideInInspector]
    public float handYpos = 0f;

    [HideInInspector]
    public float handXpos = 0f;

    [HideInInspector]
    public List<Renderer> renderers = new List<Renderer>();

    List<int> sortingOrder = new List<int>();

    public SpriteRenderer hiddenSprite;
    bool hidden = false;

    void Awake()
    {
        cachedBoxColliderSize = cardCollider.size;

        foreach (Renderer r in GetComponentsInChildren<Renderer>())
            renderers.Add(r);

        sortingOrder.Clear();
        foreach (Renderer r in this.renderers)
            sortingOrder.Add(r.sortingOrder);
    }

    public void ResetSortingOrders()
    {
        int index = 0;
        foreach (Renderer r in this.renderers)
        {
            r.sortingOrder = sortingOrder[index];
            index++;
        }
    }

    string planetSpriteName = "";
	public void SetCard(Card card, bool onPopup = false)
	{
		this.card = card;
        this.titleText.text = Card.GetTitleText(card.type);
        this.mainText.text = Card.GetMiddleText(card.type);
        this.rankText.text = Card.GetRankText(card.rank);
        this.suitSprite.sprite = Card.GetSuitSprite(card.suit);

        this.costText.text = "+" + Card.GetMomentumForRank(card.rank);

        if (Card.GetMomentumForRank(card.rank) < 1)
            this.costText.text = "";

        this.suitSprite.color = Card.GetSuitColor(card.suit);
        this.rankText.color = Card.GetSuitColor(card.suit);

        this.bgSprite.sprite = Card.GetBackgroundSprite(card.type);
        bgSprite.color = Card.GetBackgroundColor(card.type);

        this.gameObject.name = "Card: " + card.cardId + " " + card.GetCardString();
        this.timingText.text = "";
        this.numText.text = "";

        if(card.type != CardType.PlayerCard)
        {
            this.rankText.text = "";
            this.costText.gameObject.SetActive(true);
            this.costText.color = Game.GetPlayerColor(Player.You);
            this.costText.text = Card.GetMomentumCost(card.type).ToString();
            this.timingText.text = Card.GetTimingText(Card.GetCardTiming(card.type));
            this.gameObject.name = "Card: " + card.cardId + " " + card.type.ToString();
        }

        SetColliderSize(false);

        UpdateDisplay();
    }

	void OnEnable()
	{
		uiItem.OnClick += HandleCardClicked;
	}

	void OnDisable()
	{
		uiItem.OnClick -= HandleCardClicked;
	}

	public void SetVerticalPos(float yPos)
	{
		handYpos = yPos;
		TweenToPos();
	}

    public bool IsUsingCard(Player player = Player.You)
    {
        return CardManager.Instance.IsUsingCard(this, player);
    }

	public void TweenToPos()
	{
		if(IsUsingCard())
		{
			return;
		}

		transform.DOKill();
		transform.DOLocalMoveY(handYpos, 0.3f);
		transform.DOLocalMoveX(handXpos, 0.3f);
	}
	
    public bool IsOpponentsCard()
    {
        return CardManager.Instance.opponentHandCardObjects.Contains(this) || IsUsingCard(Player.Opponent);
    }

    public bool IsPlayersCard()
    {
        return CardManager.Instance.handCardObjects.Contains(this) || IsUsingCard();
    }

    public bool IsShopCard()
    {
        return Game.Instance.cardMarket.GetCastedList<CardObject>().Contains(this);
    }

    public void HandleCardClicked()
	{
		if (Game.Instance.IsGameOver() || PopupManager.Instance.BlockBoardActions())
			return;

        if (IsOpponentsCard())
            return;

        if(IsPlayersCard())
        {
            if (Game.Instance.inputState == InputState.Playing && !IsUsingCard())
            {
                CardManager.Instance.PlayCard(this);
            }
            else if (Game.Instance.inputState == InputState.PlayComboCards)
            {
                if (IsUsingCard())
                {
                    CardManager.Instance.RemoveFromAdditionalCards(this, Player.You);
                }
                else
                {
                    CardManager.Instance.AddAdditionalCard(this, Player.You);
                }
            }
            else if(Game.Instance.inputState == InputState.PlayKickCards)
            {
                CardManager.Instance.AddAdditionalCard(this, Player.You);
                CardManager.Instance.AddFlushCards(this.card.suit);
                Game.Instance.AddRandomKickCards();
            }
            else if (Game.Instance.inputState == InputState.Discarding)
            {
                CardManager.Instance.DiscardCard(this);
                Game.Instance.GoToBaseState();
            }
            else if (Game.Instance.inputState != InputState.Playing)
            {
                SoundManager.Instance.PlaySound("warning");
                //Helpers.ShowReminderText("Finish your action!");
            }
        }
        else if(IsShopCard() && Game.Instance.inputState == InputState.Playing)//is shop card
        {
            if(Game.Instance.CanAfford(Player.You, this.card))
            {
                Game.Instance.BuyCard(Player.You, this);
            }
            else
            {
                //Game.Instance.cardMarket.UpdatePositions();
                this.gameObject.transform.DOGoto(5f);
                this.transform.DOShakePosition(0.3f);
            }
        }
    }
    
    string soundName = "";
	public void Rotate(bool right = true)
	{
		int dirNum = (int)direction;
		dirNum = right ? dirNum + 1 : dirNum - 1;

		if (dirNum == 5)
			dirNum = 1;

		if (dirNum == 0)
			dirNum = 4;

		SetRotation((Direction)dirNum);


        soundName = !right ? "WCShort5" : "WCShort6";

        SoundManager.Instance.PlaySound(soundName);
	}

    public float GetZRotation()
    {
        switch(direction)
        {
            case Direction.Up:
                return 180f;
            case Direction.Left:
                return 270f;
            case Direction.Right:
                return 90f;
            case Direction.Down:
                return 0f;
        }
        return 0f;
    }

	public void SetRotation(Direction d)
	{
		direction = d;

		rotater.DOKill();

		RotateMode mode = RotateMode.Fast;

		//Debug.Log(d.ToString());
		//rotater.localRotation = Quaternion.Euler(0f, 0f, 0f);
		rotater.DORotate(new Vector3(0f, 0f, 90f), 0.3f, mode);

		switch (direction)
		{
			case Direction.Up:
				rotater.DORotate(new Vector3(0f, 0f, 180f), 0.3f, mode);
				break;
			case Direction.Left:
				rotater.DORotate(new Vector3(0f, 0f, 270f), 0.3f, mode);
				break;
			case Direction.Right:
				rotater.DORotate(new Vector3(0f, 0f, 90f), 0.3f, mode);
				break;
			case Direction.Down:
				rotater.DORotate(new Vector3(0f, 0f, 0f), 0.3f, mode);
				break;
		}
	}

    public void CleanUp()
	{
        if(this != null && this.gameObject != null)
        {
            Destroy(this.gameObject);
        }
	}

    public void UpdateDisplay()
    {
        mainText.text = Card.GetMiddleText(card.type);

        if (this.IsOpponentsCard())
        {
            costText.gameObject.SetActive(Game.Instance.defensePlayer == Player.Opponent);
            costText.color = Game.GetPlayerColor(Player.Opponent);
        }
        else if(this.IsPlayersCard())
        {
            costText.gameObject.SetActive(Game.Instance.defensePlayer == Player.You);
            costText.color = Game.GetPlayerColor(Player.You);
        }
    }

    internal void SetHidden(bool hidden)
    {
        this.hidden = hidden;
        hiddenSprite.gameObject.SetActive(hidden);
    }

    Vector3 cachedBoxColliderSize = Vector3.zero;
    internal void SetColliderSize(bool isExtended)
    {
        extended = isExtended;
        Vector3 sizeToUse = cachedBoxColliderSize;
        Vector3 centerToUse = new Vector3(0f, -3f, 0f);

        if (!isExtended)
        {
            centerToUse = Vector3.zero;
            sizeToUse.y = -6f;
        }

        cardCollider.center = centerToUse;
        cardCollider.size = sizeToUse;
    }
}
