﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Gamelogic.Grids;
using System.Linq;
using TMPro;

public enum CardType
{
    None = 0,
    PlayerCard = 1,
    
    Draw1 = 101,
    Draw2 = 102,
    Draw3 = 103, // NOW

    Fumble = 201, //DEFENSE
    Interception = 202,
    Sack3 = 203,
    Sack5 = 204,
    Sack7 = 205,

    KeepWhenBlind = 301, //DRIVE
    DriveCard = 302,

    StrongRunner = 401, //OFFENSE
    StrongPasser = 402,
    BigPlay = 403,
    BigPass = 404,
    BigRun = 405,

    SteadyOffense = 501, //GAME
}

public enum CardTiming
{
    None = 0,
    Immediate = 1,
    Drive = 2,
    Offense = 3,
    Defense = 4,
    Game = 5,
}

public enum CardSuit
{
    None = 0,
    Spades = 1,
    Clubs = 2,
    Hearts = 3,
    Diamonds = 4,
}

public enum CardRank
{
    None = 0,
    Ace = 1,
    Two = 2,
    Three = 3,
    Four = 4,
    Five = 5,
    Six = 6,
    Seven = 7,
    Eight = 8,
    Nine = 9,
    Ten = 10,
    Jack = 11,
    Queen = 12,
    King = 13,
}

public class Card
{
    public string descText = "";
    public CardType type = CardType.None;
    public CardSuit suit = CardSuit.None;
    public CardRank rank = CardRank.None;

    public int cardId = 0;
    public static int ID_GEN = 0;

    public int Value
    {
        get { return GetCardValue(); }
    }


    internal string GetDescriptionText()
    {
        return descText;
    }

    public Card(CardType type, CardSuit suit = CardSuit.None, CardRank rank = CardRank.None)
    {
        descText = "default title";
        this.type = type;
        this.suit = suit;
        this.rank = rank;
        ID_GEN++;
        cardId = ID_GEN;
    }

    public static Sprite GetBackgroundSprite(CardType type)
    {
        switch(type)
        {
            case CardType.PlayerCard:
                return Helpers.GetSprite("cardfront");
        }

        return Helpers.GetSprite("cardfront");
    }

    public static Color GetBackgroundColor(CardType type)
    {
        switch (type)
        {
            case CardType.None:
                return Color.black;
            case CardType.PlayerCard:
                return Color.white;
        }

        return Color.white;
    }


    internal static CardRank NextRank(CardRank rank)
    {
        int nextRank = (int)rank;
        nextRank++;

        if (nextRank <= 13)
            return (CardRank)nextRank;
        return CardRank.None;
    }

    internal static CardRank PrevRank(CardRank rank)
    {
        int prevRank = (int)rank;
        prevRank--;

        if (prevRank >= 1)
            return (CardRank)prevRank;
        return CardRank.None;
    }

    internal static Color GetSuitColor(CardSuit suit)
    {
        switch(suit)
        {
            case CardSuit.Clubs:
            case CardSuit.Spades:
                return Color.black;
        }

        return Color.red;
    }

    internal static Sprite GetSuitSprite(CardSuit suit)
    {
        if (suit == CardSuit.None)
            return Helpers.GetSprite("clear");

        return Helpers.GetSprite(suit.ToString().ToLower());
    }

    internal static string GetRankText(CardRank rank)
    {
        if (rank == CardRank.None)
            return "";

        switch(rank)
        {
            case CardRank.King:
                return "K";
            case CardRank.Queen:
                return "Q";
            case CardRank.Jack:
                return "J";
            case CardRank.Ace:
                return "A";
        }
        return ((int)rank).ToString();
    }

    internal string GetCardString()
    {
        return Card.GetRankText(this.rank) + this.suit.ToString().Substring(0, 1);
    }

    public static void DrawCard()
    {
        CardManager.Instance.DrawCard();
    }


    #region CARD_DEFS
    /// 
    /// 
    /// 
    /// CARD DEFS!!!
    ///
    /// 

    public static string GetTitleText(CardType type)
    {
        switch (type)
        {
            case CardType.None:
                return "";
            case CardType.PlayerCard:
                return "";
            case CardType.Draw1:
                return "Concept";
            case CardType.Draw2:
                return "Plan";
            case CardType.Draw3:
                return "Scheme";
            case CardType.KeepWhenBlind:
                return "Trick Play";
            case CardType.StrongPasser:
                return "The Cannon";
            case CardType.StrongRunner:
                return "The Bus";
            case CardType.BigPlay:
                return "Big Play";
            case CardType.BigPass:
                return "Precision Pass";
            case CardType.BigRun:
                return "Gritty Run";
            case CardType.Sack3:
                return "Sack";
            case CardType.Sack5:
                return "Big Sack";
            case CardType.Sack7:
                return "Major Sack";
            case CardType.DriveCard:
                return "Dig Deep";
            case CardType.SteadyOffense:
                return "Steady Offense";
        }

        return type.ToString();
    }

    internal static string GetMiddleText(CardType type)
    {
        if (type == CardType.PlayerCard)
            return "";

        switch(type)
        {
            case CardType.Draw1:
                return "Draw 1 Card";
            case CardType.Draw2:
                return "Draw 2 Cards";
            case CardType.Draw3:
                return "Draw 3 Cards";
            case CardType.Fumble:
                return "Force turnover if you win YAC on a run.";
            case CardType.Interception:
                return "Force turnover if you win YAC on a pass.";
            case CardType.KeepWhenBlind:
                return "The next time you draw a blind card, don't discard your hand.";
            case CardType.StrongPasser:
                return "+2 yards on each successful pass";
            case CardType.StrongRunner:
                return "+1 yard on each successful run";
            case CardType.BigPlay:
                return "+5 yards if your next play is successful";
            case CardType.BigRun:
                return "+4 yards if your next run is successful";
            case CardType.BigPass:
                return "+7 yards if your next pass is successful";
            case CardType.Sack3:
                return "-3 yards if you win you next defensive snap";
            case CardType.Sack5:
                return "-5 yards if you win you next defensive snap";
            case CardType.Sack7:
                return "-7 yards if you win you next defensive snap";
            case CardType.SteadyOffense:
                return "+1 yards on each successful offensive play";
            case CardType.DriveCard:
                return "+1 card for this drive";
        }

        return type.ToString();
    }

    public static int GetMomentumCost(CardType type)
    {
        switch(type)
        {
            case CardType.Draw1:
            case CardType.Sack3:
            case CardType.KeepWhenBlind:
                return 2;
            case CardType.Draw2:
            case CardType.Sack5 :
            case CardType.StrongRunner:
            case CardType.StrongPasser:
            case CardType.BigPlay:
            case CardType.BigPass:
            case CardType.BigRun:
                return 4;
            case CardType.Draw3:
            case CardType.Sack7:
            case CardType.Fumble:
            case CardType.Interception:
                return 6;
            case CardType.DriveCard:
                return 10;
            case CardType.SteadyOffense:
                return 20;
        }
        return 0;
    }

    public static CardTiming GetCardTiming(CardType type)
    {
        switch(type)
        {
            case CardType.Draw1:
            case CardType.Draw2:
            case CardType.Draw3:
                return CardTiming.Immediate; //IMMEDIATE
            case CardType.Fumble:
            case CardType.Interception:
            case CardType.Sack3:
            case CardType.Sack5:
            case CardType.Sack7:
                return CardTiming.Defense; //DEFENSE
            case CardType.BigPlay:
            case CardType.StrongPasser:
            case CardType.StrongRunner:
            case CardType.BigRun:
            case CardType.BigPass:
                return CardTiming.Offense; //OFFENSE
            case CardType.KeepWhenBlind:
            case CardType.DriveCard:
                return CardTiming.Drive; //DRIVE
            case CardType.SteadyOffense:
                return CardTiming.Game;
        }

        return CardTiming.None; 
    }

    internal static string GetTimingText(CardTiming cardTiming)
    {
        switch(cardTiming)
        {
            case CardTiming.Immediate:
                return "NOW";
            case CardTiming.Drive:
                return "DRIVE";
            case CardTiming.Offense:
                return "OFFENSE";
            case CardTiming.Defense:
                return "DEFENSE";
            case CardTiming.Game:
                return "GAME";
        }
        return "";
    }

    public static bool OpponentCanPurchase(CardType type)
    {
        switch(type)
        {
            case CardType.KeepWhenBlind:
                return false;
        }
        return true;
    }

    public static int GetMomentumForRank(CardRank rank)
    {
        switch(rank)
        {
            case CardRank.Four:
                return 2;
            case CardRank.Three:
                return 1;
            case CardRank.Six:
                return 1;
        }
        return 0;
    }

    //internal static Cost GetCost(CardType type)
    //{
    //    switch(type)
    //    {
    //        case CardType.None:
    //        return new Cost(0, 0, 0, 0, 0);
    //    }
    //    return new Cost(0, 0, 0, 0, 0);
    //}

    internal PlayType GetPlayType()
    {
        if (Value <= 5)
            return PlayType.Run;

        return PlayType.Pass;
    }

    private int GetCardValue()
    {
        if ((int)rank <= 10)
            return (int)rank;

        return 10;
    }

    public static List<ComboType> GetCombos(List<Card> cards)
    {
        if (cards.Count < 2)
            return new List<ComboType>();

        cards.Sort((x, y) => x.rank.CompareTo(y.rank));

        List<ComboType> combos = new List<ComboType>() { ComboType.Flush, ComboType.Straight, ComboType.XofAKind };

        CardSuit suit = cards[0].suit;
        foreach(Card c in cards)
        {
            if (suit != c.suit)
                combos.Remove(ComboType.Flush);
        }

        CardRank rank = cards[0].rank;
        foreach (Card c in cards)
        {
            if (rank != c.rank)
                combos.Remove(ComboType.XofAKind);
        }

        if (!CheckStraight(cards))
            combos.Remove(ComboType.Straight);

        //if(cards[0].rank == CardRank.Ace) //ace high straight
        //{
        //    cards.RemoveAt(0);
        //    if (CheckStraight(cards) && cards.Last().rank == CardRank.King)
        //        combos.Add(ComboType.Straight);
        //}

        return combos;
    }

    internal static void ProcessImmediateAbility(CardType type, Player player, float delay)
    {
        Game.Instance.CallActionDelayed(delegate ()
        {
            ProcessImmediateAbility(type, player);
        }, delay);
    }

    internal static void ProcessImmediateAbility(CardType type, Player player)
    {
        switch (type)
        {
            case CardType.Draw1:
                CardManager.Instance.DrawCard(player);
                break;
            case CardType.Draw2:
                CardManager.Instance.DrawCard(player);
                CardManager.Instance.DrawCard(player);
                break;
            case CardType.Draw3:
                CardManager.Instance.DrawCard(player);
                CardManager.Instance.DrawCard(player);
                CardManager.Instance.DrawCard(player);
                break;
        }
    }

    public static bool CheckStraight(List<Card> cards)
    {
        CardRank rank = cards[0].rank;
        foreach (Card c in cards) 
        {
            if (rank != c.rank)
                return false;

            int nextRank = (int)rank + 1;
            if (nextRank > 13 && c != cards.Last())
                return false;

            rank = (CardRank)nextRank;
        }

        return true;
    }

    public static int CompareCombo(List<Card> one, List<Card> two)
    {
        if (one.Count > two.Count)
            return 1; //one has more cards and is better

        if (one.Count < two.Count)
            return -1; //two has more cards and is better

        if ((int)GetBestComboType(one) > (int)GetBestComboType(two))
            return 1; //one has better combo type

        if ((int)GetBestComboType(one) < (int)GetBestComboType(two))
            return -1; //one has better combo type

        if (GetHighCardRank(one) > GetHighCardRank(two))
            return 1; //one has higher card

        if (GetHighCardRank(one) < GetHighCardRank(two))
            return -1; //two has higher card

        return 0;
    }

    public static ComboType GetBestComboType(List<Card> cards)
    {
        List<ComboType> combos = Card.GetCombos(cards);
        if (combos.Count > 0)
            return combos.Last();

        return ComboType.None;
    }

    public static int GetHighCardRank(List<Card> cards)
    {
        int highCardValue = 0;
        foreach(Card c in cards)
        {
            if ((int)c.rank > highCardValue)
                highCardValue = (int)c.rank;
        }
        return highCardValue;
    }

    internal bool IsRun()
    {
        return GetPlayType() == PlayType.Run;
    }

    #endregion


}
